# main.tf
terraform {
  backend "http" {
  }
}

resource "azurerm_resource_group" "this" {
  name     = "test-rg-gitlab"
  location = "westeurope"

}

resource "azurerm_container_group" "this" {
  location            = azurerm_resource_group.this.location
  resource_group_name = azurerm_resource_group.this.name
  name                = "aci-abbdemo-dev828342"
  ip_address_type     = "Public"
  dns_name_label      = "aci-abbdemo-dev828342"
  os_type             = "Linux"
  restart_policy      = "Never"

  container {
    name   = "hello-world"
    image  = "mcr.microsoft.com/azuredocs/aci-helloworld:latest"
    cpu    = "0.5"
    memory = "1.5"

    ports {
      port     = 80
      protocol = "TCP"
    }
  }

  container {
    name   = "sidecar"
    image  = "mcr.microsoft.com/azuredocs/aci-tutorial-sidecar"
    cpu    = "0.5"
    memory = "1.5"
  }

}
